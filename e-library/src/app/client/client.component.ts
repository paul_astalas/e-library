import { Component, OnInit } from '@angular/core';
import { ClientModel } from '../models/client.model';
import { BookModel } from '../models/book.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../rest_service/rest_service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css'],
  providers: [DataService]
})
export class ClientComponent implements OnInit {

  private client: ClientModel;
  private searchResultBooks: any;
  private pages: string[];
  private currentPage: string;
  private bookForUpdateGrade : BookModel;
  private curentGrade: number;

  constructor(private router: Router, private route: ActivatedRoute, private service: DataService) {
    this.client = new ClientModel();
    this.bookForUpdateGrade = new BookModel();
    
    this.pages = ["about-us","literary-realism","poetry","drama","romance",
            "fantasy","economy","science","philosophy","showQueryResult","read-book"];
  }

  ngOnInit() {
    console.log("Initializing client model");

    let handleClient = (client) => {
        this.client = client;
    }

    let handleError = (error) => {
        this.router.navigate(['']);
    }

    this.route.params.subscribe (
        (params) => { this.service.getClientDetails(params['id']).subscribe(handleClient, handleError); }
    )

    

    document.getElementById("innerSearchBox").style.display = "none";
    document.getElementById("innerGenreBox").style.display = "none";

    this.currentPage = "about-us";

    let i;
    for (i = 0; i < this.pages.length; i++) 
    { 
      document.getElementById(this.pages[i]).style.display = "none";
    }
  }

  public logout() {
    var handleLogout = (res) => {
        if (res >= 0) {
            this.router.navigate(['login', res]);
        }
    }

    this.service.logout(this.client).subscribe(handleLogout);
  }

  searchBooks(searchParameters) {
    console.log(searchParameters);
    let handleBooks = books => {console.log(books); this.searchResultBooks = books;};
    this.service.getBooksByField(
        searchParameters.type,
        searchParameters.key
    ).subscribe(handleBooks);
  } 

  updateGrade(formValue)
  {
    this.curentGrade = formValue; // verifica folosirea acestei variabile
    this.service.updateBookGrade(formValue.grade,this.bookForUpdateGrade).subscribe((resp)=>{this.listAllBooks();});
  }

  checkGradeValue()
  {
    if(this.curentGrade<0 || this.curentGrade>10)
    {
      return false;
    }
    return true;
  }


  toggleVisualisationSearch()
  {
    var x = document.getElementById("innerSearchBox");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
  }

  toggleVisualisationGenres()
  {
    var x = document.getElementById("innerGenreBox");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
  }

  goToAboutUsPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[0]).style.display = "block";
    this.currentPage = this.pages[0];
  }

  goToLiteraryRealismPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[1]).style.display = "block";
    this.currentPage = this.pages[1];
  }

  goToPoetryPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[2]).style.display = "block";
    this.currentPage = this.pages[2];
  }

  goToDramaPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[3]).style.display = "block";
    this.currentPage = this.pages[3];
  }

  goToRomancePage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[4]).style.display = "block";
    this.currentPage = this.pages[4];
  }

  goToFantasyPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[5]).style.display = "block";
    this.currentPage = this.pages[5];
  }

  goToEconomyPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[6]).style.display = "block";
    this.currentPage = this.pages[6];
  }

  goToSciencePage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[7]).style.display = "block";
    this.currentPage = this.pages[7];
  }

  goToPhilosophyPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[8]).style.display = "block";
    this.currentPage = this.pages[8];
  }

  goToShowQueryResultPage()
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[9]).style.display = "block";
    this.currentPage = this.pages[9];
  }

  listAllBooks()
  {
    this.goToShowQueryResultPage();
    let handleBooks = books => {console.log(books); this.searchResultBooks = books;};
    this.service.getAllBooks().subscribe(handleBooks);
  }

  readSelectedBook(book)
  {
    document.getElementById(this.currentPage).style.display = "none";
    document.getElementById(this.pages[10]).style.display = "block";
    this.currentPage = this.pages[10];

    this.bookForUpdateGrade = book;

    document.getElementById("openPDF").innerHTML = '<object class="textArea" data=' 
                                                           + book.link + '></object>' + 
                                                           '<style>\
                                                           .textArea\
                                                           {\
                                                               margin: 0px;\
                                                               width: 1100px;\
                                                               height: 450px;\
                                                               border: 3px solid black;\
                                                           }\
                                                           </style>';
  }

}
