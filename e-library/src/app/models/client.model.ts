export class ClientModel {
    username: string;
    password: string;
    firstname: string;
    lastname: string;
    favouriteGenre: string;
    email: string;

    constructor()
    {

    }
}