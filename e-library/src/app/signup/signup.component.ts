import { Component, OnInit} from '@angular/core';
import { DataService} from '../rest_service/rest_service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [DataService]
})
export class SignupComponent implements OnInit {

  private searchResultClients: any;
  private usernameAlreadyExists: boolean;

  constructor(private service: DataService, private router: Router) { }

  ngOnInit() {
    document.getElementById("passwordAgainMessage").style.display = "none";
    document.getElementById("usernameAlreadyExists").style.display = "none";
    this.usernameAlreadyExists = false;
    this.searchResultClients = [];
  }


  signup(client)
  {
    let onClientLoggedIn = (res) => {
      if (res >= 0) {
        this.router.navigate(['client', res]);
      }
    }

    let onClientSubscribed = (clientId) => {
      this.service.login(client).subscribe(onClientLoggedIn);
    }

    if(!(client.password==client.passwordAgain))
    {
      document.getElementById("passwordAgainMessage").style.display = "block";
    }
    else
    {
        let onClientsFetched = clients => {
          clients.every((client_item) => 
          {
            console.log(client_item.username);
            if(client_item.username == client.username)
            {
              this.usernameAlreadyExists = true;
            }
          });
          
          if(this.usernameAlreadyExists == false){
            console.log("Subscribing Client");
            this.service.postClient(client).subscribe(onClientSubscribed);
          }
          else {
            document.getElementById("messageSignupFailed").innerHTML = "the username is already taken";
          }
        }
        this.service.getAllClients().subscribe(onClientsFetched);
    }

    
  }

}
