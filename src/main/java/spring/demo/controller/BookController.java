package spring.demo.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import spring.demo.entities.Book;
import spring.demo.services.BookService;
import spring.demo.services.LoginService;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/book")
@RestController
public class
BookController {
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	BookService bookService;

	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ResponseEntity<?> listBooks(HttpServletRequest request) {
		if (loginService.checkLoggedIn(request)) {
			return new ResponseEntity<List<Book>>(bookService.findAllBooks(), HttpStatus.OK);
		}
		return new ResponseEntity<String>("unauthorized", HttpStatus.UNAUTHORIZED);
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String postBook(@RequestBody Book book)
	{
		bookService.saveBook(book);
		return "" + book.getId();
	}

	@RequestMapping(value="/query", method=RequestMethod.GET)
	public ResponseEntity<?> listBooksByCriterion(@RequestParam Map<String, String> allParameters, HttpServletRequest request) {
		if (loginService.checkLoggedIn(request)) {
			return new ResponseEntity<List<Book>>(bookService.findBooks(allParameters), HttpStatus.OK);
		}
		return new ResponseEntity<String>("unauthorized", HttpStatus.UNAUTHORIZED);
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public String deleteBook(@RequestBody Book book) {
			bookService.deleteBookById(book.getId());
			return "" + book.getId();
			
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public String updateGradeOfBook(@RequestBody BookGrade data) {
			
			Float gradeReceived = data.getGrade();
			Integer bookReceivedId = data.getBookId();
			Book originalBook = bookService.findById(bookReceivedId);
			
			Float originalGrade = originalBook.getGrade();
			Integer originalNrReviews = originalBook.getNrReviews();
			Integer newNrReviews = originalNrReviews + 1;
			
			Float newGrade = ( originalNrReviews * originalGrade + gradeReceived)/newNrReviews;
			
			originalBook.setGrade(newGrade);
			originalBook.setNrReviews(newNrReviews);
			
			bookService.saveBook(originalBook);
			
			return "" + originalBook.getId();
			
	}
	
	
	
}
