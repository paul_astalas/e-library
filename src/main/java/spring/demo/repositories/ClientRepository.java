package spring.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.Client;

public interface ClientRepository extends JpaRepository<Client, Integer>{
	public Client findByUsernameAndPassword(String username, String password);
	public Client findById(int id);
	public List<Client> findAll();
}
